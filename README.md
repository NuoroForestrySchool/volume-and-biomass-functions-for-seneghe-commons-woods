# Volume and Biomass functions for Seneghe commons woods

## Objective

Process the sample tree measurements collected in Seneghe (OR, Italy) il the years 2006-2008 to produce volume, weight and biaomass functions for the major species present in the local commons woods.

## Authors and acknowledgment

Measurements have benne collected, under the guidance of Cristian Ibba, by a small team of local workers, Angelo Cabiddu and, Maurizio Laconi, who used the data within his doctoral thesis \<titolo e riferimento\>

## License

\*\*\* CC BY-SA 4.0 DEED \*\*\*

<https://creativecommons.org/licenses/by-sa/4.0/deed.it>

## Project status

Under development.
